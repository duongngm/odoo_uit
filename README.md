# Odoo 15 for UIT Student PROJECT

Guideline to run and develop project base on image docker

The base images does nothing alone, it only contains the dependencies and a few
tools, but you need to create an inheriting image with the odoo and addons
code.


## Quick reference

-	**Based on image**:
	[Docker Odoo Offical](https://hub.docker.com/_/odoo)

## Getting Started

These instructions will get you containers of the project up and running on
your local machine for development and testing purposes.

### Prerequisites

-	[Docker](https://github.com/docker/docker-install)
-	[Docker-compose](https://docs.docker.com/compose/install)

### How to setup

-	**Example Project Docker**:
	[Example Project Docker](https://github.com/camptocamp/docker-odoo-project/tree/master/example)

-	**Configuration**:
	[configuration-options](https://github.com/camptocamp/docker-odoo-project#odoo-configuration-options),
	[odoo.cfg.tmpl](https://github.com/camptocamp/docker-odoo-project/blob/master/11.0/templates/odoo.cfg.tmpl)

-	**Requirements**: add required python libs in file *requirements.txt*

### How to use

-	**Reference**:
	[runtest, runmigration](https://github.com/camptocamp/docker-odoo-project/tree/b972737be735d47e15b419a30b0a3a478830bc11#running-tests),

#### Build and Run Odoo instance

- Build Odoo instance. If you want to build and run odoo in backgrounds,
	add `-d` to run in detach (-d) mode.

```console
$ docker-compose up
$ docker-compose up -d
$ docker-compose up --build		# Rebuild containers
```

- To use different ports for Odoo, Postgres:

```console
$ ODOO_PORT=8070 POSTGRES_PORT=5433 docker-compose up
```
For more informations about defined variables, take a look at file docker-compose-tmpl.yml


#### View logs when run in detach mode:

```console
$ docker-compose logs -f --tail=50
```

#### Upgrade Odoo instance:

```console
$ docker-compose restart
$ docker-compose run --rm odoo testdb-update -u <update_addons>
```

#### Remove Odoo instance

To keep cache volume, omit `-v`
```
$ docker-compose down -v
```
