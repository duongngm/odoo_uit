FROM odoo:15.0
MAINTAINER Duong Nguyen <duongngm@gmail.com>

# Install necessary packages
USER root
RUN set -x; \
    apt-get update \
    && apt-get install -y --no-install-recommends git \
    && pip3 install --upgrade pip

# Install python libs requirement
COPY ./requirements.txt /odoo/local-requirements/
RUN pip3 install --no-cache-dir -r /odoo/local-requirements/requirements.txt
